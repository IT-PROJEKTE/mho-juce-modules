/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

static std::unique_ptr<AudioDeviceManager> sharedAudioDeviceManager;

//==============================================================================
struct MetronomDemo : public Component, public ChangeListener, private Button::Listener {
    MetronomDemo() : audioDeviceManager(MainComponent::getSharedAudioDeviceManager()),
                     metronom(audioDeviceManager, BinaryData::clickorig_wav, BinaryData::clickorig_wavSize) {
        setName("Metronom");
        addAndMakeVisible(playStopBtn);
        playStopBtn.addListener(this);
        metronom.addChangeListener(this);

        audioDeviceManager.playTestSound();
    }

    ~MetronomDemo() {
        metronom.removeChangeListener(this);
        playStopBtn.removeListener(this);
    }

    void resized() override {
        auto rc = getLocalBounds().reduced(8).removeFromLeft(100);

        playStopBtn.setBounds(rc.removeFromTop(20));
        rc.removeFromTop(8);
        //stopBtn.setBounds(rc.removeFromTop(20));
    }

    void buttonClicked(Button *button) override {
        if (button == &playStopBtn) {
            Logger::outputDebugString("Metronom Play/Stop Button pressed");
            metronom.setTempo(120);
            metronom.playStop();
        }
    }

    void changeListenerCallback(ChangeBroadcaster *source) override {
        Logger::outputDebugString("change received");
        if (source == &metronom) {
            if (metronom.isPlaying()) {
                playStopBtn.setButtonText("Stop");
            } else {
                playStopBtn.setButtonText("Play");
            }
        }
    }

    TextButton playStopBtn{"Play"};

    AudioDeviceManager &audioDeviceManager;

    mho::Metronom metronom;
};

//==============================================================================
struct MultitrackPlayerDemo : public Component, public ChangeListener, private Button::Listener, private Slider::Listener {
    MultitrackPlayerDemo() : audioDeviceManager(MainComponent::getSharedAudioDeviceManager()),
                             multitrackPlayer(audioDeviceManager) {
        setName("MultitrackPlayer");
        addAndMakeVisible(playStopBtn);
        playStopBtn.addListener(this);
        addAndMakeVisible(fadeoutBtn);
        fadeoutBtn.addListener(this);
        addAndMakeVisible(gain);
        gain.setRange(0.0, 1.f);
        gain.setValue(1.f);
        gain.addListener(this);
                                
        multitrackPlayer.addChangeListener(this);

        audioDeviceManager.playTestSound();

        setTracks();
    }
    
    struct FadeOutThread : private Thread, private AsyncUpdater {
        FadeOutThread(mho::MultitrackPlayer &mtp_) : Thread ("fadeout"), mtp(mtp_)   {
            startThread(); }
        ~FadeOutThread() { stopThread (1000); }
        
        mho::MultitrackPlayer &mtp;

        void run() override
        {
            float gain = 1.f;
            while (gain >= 0) {
                gain -= 0.001f;
                mtp.setGain(gain);
                wait(1);
            }
            mtp.playStop();
            triggerAsyncUpdate();
        }

        void handleAsyncUpdate() override
        {
            delete this;
        }
    };
    
    void fadeout() {
        new FadeOutThread(multitrackPlayer);
        
        /*
        for (int i=0; i<=10000; i++) {
            float gain = 1.f - (1/10000);
            multitrackplayer_->setGain(gain);
            usleep(100);
        }
        multitrackplayer_->playStop();
         */
    }
    
    void sliderValueChanged (Slider* slider) override {
        if (slider == &gain) {
            multitrackPlayer.setGain(gain.getValue());
        }
        //std::thread t1(fadeout, multitrackPlayer);
        //fadeout(&multitrackPlayer);
    }
    
    void resized() override {
        auto rc = getLocalBounds().reduced(8).removeFromLeft(100);
        playStopBtn.setBounds(rc.removeFromTop(20));
        fadeoutBtn.setBounds(rc.removeFromTop(20));
        gain.setBounds (rc.removeFromTop(50).withRight(400));
        rc.removeFromTop(8);
        //stopBtn.setBounds(rc.removeFromTop(20));
    }

    void buttonClicked(Button *button) override {
        if (button == &playStopBtn) {
            Logger::outputDebugString("MultitrackPlayer Play/Stop Button pressed");

            if (multitrackPlayer.isPlaying()) {
                multitrackPlayer.playStop();
            } else {
                multitrackPlayer.setTrackTypeToPlay(mho::TrackType::PLAYBACK);
                multitrackPlayer.setTracks(tracks);
                multitrackPlayer.playStop();
            }
        }
        
        if (button == &fadeoutBtn) {
            Logger::outputDebugString("MultitrackPlayer Fadeout Button pressed");
            fadeout();
        }
    }

    void changeListenerCallback(ChangeBroadcaster *source) override {
        Logger::outputDebugString("change received");
        if (source == &multitrackPlayer) {
            if (multitrackPlayer.isPlaying()) {
                playStopBtn.setButtonText("Stop");
            } else {
                playStopBtn.setButtonText("Play");
            }
            gain.setValue(multitrackPlayer.getGain());
        }
    }

    void setTracks() {
        tracks.add(getTrack("/Users/marcus/Downloads/CallingLlyr-Playback.wav"));
        tracks.add(getTrack("/Users/marcus/Downloads/CallingLlyr-Click.wav"));
    }

    mho::Track getTrack(String url) {
        mho::Track track = mho::Track{};
        track.pan = 0.0;
        track.url = url;
        track.trackType = mho::TrackType::PLAYBACK;

        mho::Channel channel1 = mho::Channel{};
        mho::Channel channel2 = mho::Channel{};

        channel1.inChannel = 0;
        channel1.outChannel = 0;

        channel2.inChannel = 1;
        channel2.outChannel = 1;

        track.channels.add(channel1);
        track.channels.add(channel2);

        return track;
    }

    TextButton playStopBtn{"Play"};
    TextButton fadeoutBtn{"Fade"};
    
    Slider gain;

    AudioDeviceManager &audioDeviceManager;

    mho::MultitrackPlayer multitrackPlayer;

    Array<mho::Track> tracks;
};

//==============================================================================
MainComponent::MainComponent() {
    demoComponents.add(new MetronomDemo());
    demoComponents.add(new MultitrackPlayerDemo());

    for (auto *c : demoComponents)
        addChildComponent(c);

    demoList.setModel(this);
    demoList.updateContent();
    demoList.selectRow(0);
    addAndMakeVisible(demoList);

    setSize(600, 400);

}

MainComponent::~MainComponent() {
    demoComponents.remove(0);
    demoComponents.remove(1);
    demoComponents.clearQuick(true);
}

void MainComponent::paint(Graphics &g) {
    g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
}

void MainComponent::resized() {
    auto rc = getLocalBounds();

    demoList.setBounds(rc.removeFromLeft(150));

    for (auto *c : demoComponents)
        c->setBounds(rc);
}

void MainComponent::paintListBoxItem(int row, Graphics &g, int w, int h, bool rowIsSelected) {
    Rectangle<int> rc(0, 0, w, h);
    if (rowIsSelected) {
        g.setColour(Colours::lightblue);
        g.fillAll();
    }

    g.setColour(findColour(ListBox::textColourId));
    g.drawText(demoComponents[row]->getName(), rc.reduced(2), Justification::centredLeft);
}

void MainComponent::selectedRowsChanged(int lastRowSelected) {
    for (int i = 0; i < demoComponents.size(); i++)
        demoComponents[i]->setVisible(i == lastRowSelected);
}

AudioDeviceManager &MainComponent::getSharedAudioDeviceManager() {
    if (sharedAudioDeviceManager == nullptr) {
        sharedAudioDeviceManager = std::make_unique<AudioDeviceManager>();
        //    sharedAudioDeviceManager = new AudioDeviceManager();
        sharedAudioDeviceManager->initialise(0, 3, nullptr, true);
    }

    return *sharedAudioDeviceManager;
}
