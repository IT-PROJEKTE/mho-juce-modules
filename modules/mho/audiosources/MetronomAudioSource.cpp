/*
==============================================================================

 Copyright 2018 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

==============================================================================
*/

/*
 * https://github.com/jhochenbaum/nuance/blob/master/Source/Metronome.cpp
 */

MetronomAudioSource::MetronomAudioSource(const char *sampleWave_, const int sampleSize_) : sampleWave(sampleWave_),
                                                                                           sampleSize(sampleSize_) {
    formatManager.registerBasicFormats();
    
    metroReader = std::unique_ptr<AudioFormatReader>(formatManager.createReaderFor(
        std::unique_ptr<InputStream> (
            new MemoryInputStream(sampleWave, sampleSize, false))));
    
    BigInteger notes;
    notes.setRange(60, 60, true);

    samplerSynth.addVoice(new SamplerVoice());
    samplerSynth.addSound(new SamplerSound("metro", *metroReader, notes, 60, 0.0, 0.0, 1.0));
}

MetronomAudioSource::~MetronomAudioSource() {
    releaseResources();
}

void MetronomAudioSource::releaseResources() {
    samplerSynth.clearSounds();
    samplerSynth.clearVoices();
    metroReader = nullptr;
}

void MetronomAudioSource::prepareToPlay(int samplesPerBlockExpected, double sampleRate) {
    sample_rate = sampleRate;
    samplerSynth.setCurrentPlaybackSampleRate(sampleRate);
}

void MetronomAudioSource::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill) {
    // the synth always adds its output to the audio buffer, so we have to clear it
    // first..
    bufferToFill.clearActiveBufferRegion();

    int const numSamples = bufferToFill.numSamples;

    MidiBuffer midi;
    if (active) {
        double const samplesPerBeat = sample_rate * 60 / tempo;

        // Adjust phase so the beat is on or after the beginning of the output
        double beat = 0;
        if (clockPhase > 0)
            beat = 1 - clockPhase;
        else
            beat = 0 - clockPhase;

        for (;; beat += 1) {
            int pos = static_cast<int>(beat * samplesPerBeat);
            if (pos < numSamples) {
                midi.addEvent(MidiMessage::noteOn(1, 60, 1.f), pos);
            } else {
                break;
            }
        }
        advanceClock(numSamples);
    }
    samplerSynth.renderNextBlock(*bufferToFill.buffer, midi, 0, bufferToFill.numSamples);
}

void MetronomAudioSource::advanceClock(int numSamples) {
    double const samplesPerBeat = 44100 * 60 / tempo;

    jassert(clockPhase >= -.5 && clockPhase < .5);

    if (clockPhase < 0)
        clockPhase = clockPhase + 1;

    clockPhase = clockPhase + numSamples / samplesPerBeat;

    if (clockPhase >= .5)
        clockPhase -= 1;

    jassert(clockPhase >= -.5 && clockPhase < .5);
}

void MetronomAudioSource::start() {
    if (!active) {
        active = true;
        sendChangeMessage();
    }
}

void MetronomAudioSource::stop() {
    if (active) {
        active = false;
        sendChangeMessage();
    }
}

bool MetronomAudioSource::isActive() {
    return active;
}

void MetronomAudioSource::setTempo(int bpm) {
    tempo = (double) bpm;
}
