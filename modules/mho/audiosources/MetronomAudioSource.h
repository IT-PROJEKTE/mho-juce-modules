/*==============================================================================

 Copyright 2018 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

 ==============================================================================*/

#pragma once

class MetronomAudioSource : public AudioSource,
                            public ChangeBroadcaster {

public:
    MetronomAudioSource(const char *sampleWave_, const int sampleSize_);

    ~MetronomAudioSource();

    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;

    void releaseResources() override;

    void getNextAudioBlock(const AudioSourceChannelInfo &) override;

    void start();

    void stop();

    bool isActive();

    void setTempo(int bpm);

private:

    AudioFormatManager formatManager;

    Synthesiser samplerSynth;

    bool active = false;
    double sample_rate;
    double tempo = 1.;
    double clockPhase = 0;

    void advanceClock(int numSamples);

    const char *sampleWave;
    const int sampleSize;

    std::unique_ptr<AudioFormatReader> metroReader;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MetronomAudioSource);

};

