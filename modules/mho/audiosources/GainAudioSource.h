/*==============================================================================

 Copyright 2019 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

 ==============================================================================*/

#pragma once

class GainAudioSource : public AudioSource {

public:
    GainAudioSource(AudioSource *const source_, const bool deleteSourceWhenDeleted);

    ~GainAudioSource();

    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    
    void releaseResources() override;
    
    void getNextAudioBlock(const AudioSourceChannelInfo &) override;

    void setGain(float gain_);
    
    float getGain();
private:
    OptionalScopedPointer<AudioSource> source;
    //CriticalSection lock;
    float gain = 1.f;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GainAudioSource);
};
