
#include "GainAudioSource.h"

GainAudioSource::GainAudioSource(AudioSource *const source_, const bool deleteSourceWhenDeleted) : source(source_, deleteSourceWhenDeleted) {
}

GainAudioSource::~GainAudioSource(){}

void GainAudioSource::prepareToPlay(int samplesPerBlockExpected, double sampleRate) {
    source->prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void GainAudioSource::releaseResources() {
    source->releaseResources();
}

void GainAudioSource::setGain(float gain_) {
    gain = gain_;
}

float GainAudioSource::getGain() {
    return gain;
}

void GainAudioSource::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill) {
    source->getNextAudioBlock(bufferToFill);
    
    
    bufferToFill.buffer->applyGain(gain);
    /*
    bufferToFill.buffer->applyGainRamp(0, bufferToFill.startSample, bufferToFill.numSamples, gain, gain);
    bufferToFill.buffer->applyGainRamp(1, bufferToFill.startSample, bufferToFill.numSamples, gain, gain);
    */
}
