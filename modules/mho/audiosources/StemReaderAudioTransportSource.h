/*==============================================================================

 Copyright 2018 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

 ==============================================================================*/

#pragma once

class StemReaderAudioTransportSource : public AudioTransportSource {

public:
    StemReaderAudioTransportSource();

    ~StemReaderAudioTransportSource();

    void setAudioFormatReader(AudioFormatReader *audioFormatReader);

private:
    std::unique_ptr<AudioFormatReaderSource> readerSource;
    juce::TimeSliceThread readerThread;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (StemReaderAudioTransportSource);
};

