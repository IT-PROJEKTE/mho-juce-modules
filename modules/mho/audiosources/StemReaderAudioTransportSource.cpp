/*
==============================================================================

 Copyright 2018 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

==============================================================================
*/

StemReaderAudioTransportSource::StemReaderAudioTransportSource() : readerThread("Stem Reader Thread") {
    readerThread.startThread(5);
}

StemReaderAudioTransportSource::~StemReaderAudioTransportSource() {
    setSource(nullptr);
}

void StemReaderAudioTransportSource::setAudioFormatReader(AudioFormatReader *audioFormatReader) {
    readerSource = std::make_unique<AudioFormatReaderSource>(audioFormatReader, true);
    setSource(readerSource.get(), 512 * 1024, &readerThread, audioFormatReader->sampleRate);
}
