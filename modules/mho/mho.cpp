/*==============================================================================

 Copyright 2019 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

 ==============================================================================*/

// Your project must contain an AppConfig.h file with your project-specific settings in it,
// and your header search path must make it accessible to the module's files.
#include "AppConfig.h"

//==============================================================================

#ifdef  _WIN32
#include <Windows.h>
#include <ctime>
#endif
#ifdef __linux__

#include <sys/inotify.h>
#include <limits.h>
#include <unistd.h>

#endif
#ifdef __APPLE__
#import <Foundation/Foundation.h>
#endif

#ifndef  _WIN32

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <ctime>

#endif

#include "mho.h"

//==============================================================================

#if __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wshadow"
#pragma clang diagnostic ignored "-Wunused-parameter"
#endif

#if _MSC_VER
#pragma warning (push)
#pragma warning (disable: 4100)
#pragma warning (disable: 4127)
#pragma warning (disable: 4456)
#pragma warning (disable: 4457)
#endif

#if __clang__
#pragma clang diagnostic pop
#endif

#if _MSC_VER
#pragma warning (pop)
#endif

namespace mho {
    using namespace juce;

#include "components/Metronom.cpp"
#include "components/Multitrackplayer.cpp"
#include "audiosources/MetronomAudioSource.cpp"
#include "audiosources/StemReaderAudioTransportSource.cpp"
#include "audiosources/GainAudioSource.cpp"

}
