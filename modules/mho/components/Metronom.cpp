/*==============================================================================

 Copyright 2018 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

 ==============================================================================*/

Metronom::Metronom(AudioDeviceManager &audioDeviceManager_, const char *sampleWave_, const int sampleSize_)
        : audioDeviceManager(audioDeviceManager_),
          sampleWave(sampleWave_), sampleSize(sampleSize_) {
    Logger::outputDebugString("Metronom");

    metronomAudioSource = std::make_unique<MetronomAudioSource>(sampleWave, sampleSize);
    channelRemappingAudioSource = std::make_unique<ChannelRemappingAudioSource>(metronomAudioSource.get(), false);

    audioSourcePlayer.setSource(&mixerAudioSource);
    audioDeviceManager.addAudioCallback(&audioSourcePlayer);
    audioDeviceManager.addChangeListener(this);
    metronomAudioSource->addChangeListener(this);

    state = Stopped;
}

Metronom::~Metronom() {

    audioDeviceManager.removeChangeListener(this);
    audioDeviceManager.removeAudioCallback(&audioSourcePlayer);
    audioSourcePlayer.setSource(nullptr);
}

void Metronom::changeListenerCallback(juce::ChangeBroadcaster *source) {
    if (metronomAudioSource.get() == source) {
        if (metronomAudioSource->isActive()) {
            changeState(Playing);
        } else {
            if ((Stopping == state) || (Playing == state)) {
                changeState(Stopped);
            }
        }
    }

    sendChangeMessage();
}

void Metronom::playStop() {
    if (Stopped == state) {
        changeState(Starting);
    } else if (Playing == state) {
        changeState(Stopping);
    } else {
        changeState(Stopping);
    }
}

bool Metronom::isPlaying() {
    if ((Playing == state) || (Starting == state)) {
        return true;
    }
    return false;
}

void Metronom::setTempo(int bpm) {
    mixerAudioSource.removeAllInputs();

    metronomAudioSource->setTempo(bpm);

    channelRemappingAudioSource->setNumberOfChannelsToProduce(2);
    channelRemappingAudioSource->clearAllMappings();
    channelRemappingAudioSource->setInputChannelMapping(0, 0);
    channelRemappingAudioSource->setOutputChannelMapping(1, 0);

    mixerAudioSource.addInputSource(channelRemappingAudioSource.get(), false);
}

void Metronom::changeState(TransportState newState) {
    if (state != newState) {
        state = newState;
        switch (state) {
            case Stopped:
                break;
            case Starting:
                metronomAudioSource->start();
                break;
            case Playing:
                break;
            case Stopping:
                metronomAudioSource->stop();
                break;
        }
    }
}