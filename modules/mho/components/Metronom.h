/*==============================================================================

 Copyright 2018 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

 ==============================================================================*/

#pragma once

#include "../audiosources/MetronomAudioSource.h"

//==============================================================================*/
class Metronom : public Component,
                 public ChangeListener,
                 public ChangeBroadcaster {
public:
    Metronom(AudioDeviceManager &audioDeviceManager_, const char *sampleWave_, const int sampleSize_);

    ~Metronom();

    void changeListenerCallback(ChangeBroadcaster *source) override;

    void playStop();

    bool isPlaying();

    void setTempo(int bpm);

protected:


private:
    enum TransportState {
        Stopped,
        Starting,
        Playing,
        Stopping
    };

    TransportState state;
    AudioDeviceManager &audioDeviceManager;
    MixerAudioSource mixerAudioSource;
    AudioSourcePlayer audioSourcePlayer;

    std::unique_ptr<MetronomAudioSource> metronomAudioSource;
    std::unique_ptr<ChannelRemappingAudioSource> channelRemappingAudioSource;

    const char *sampleWave;
    const int sampleSize;

    void changeState(TransportState newState);

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Metronom)

};
