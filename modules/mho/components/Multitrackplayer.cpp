/*
==============================================================================

 Copyright 2018 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

==============================================================================
*/

MultitrackPlayer::MultitrackPlayer(AudioDeviceManager &audioDeviceManager_) : audioDeviceManager(audioDeviceManager_) {
    Logger::outputDebugString("MultitrackPlayer");

    formatManager.registerBasicFormats();

    audioSourcePlayer.setSource(&mixerAudioSource);
    audioDeviceManager.addAudioCallback(&audioSourcePlayer);
    audioDeviceManager.addChangeListener(this);

    state = Stopped;
}

MultitrackPlayer::~MultitrackPlayer() {
    unloadFormerTracks();

    audioDeviceManager.removeChangeListener(this);
    audioDeviceManager.removeAudioCallback(&audioSourcePlayer);
    audioSourcePlayer.setSource(nullptr);
    formatManager.clearFormats();
}

void MultitrackPlayer::changeListenerCallback(ChangeBroadcaster *src) {
    if (&audioDeviceManager == src) {
        AudioDeviceManager::AudioDeviceSetup setup;
        audioDeviceManager.getAudioDeviceSetup(setup);

        if (setup.outputChannels.isZero()) {
            audioSourcePlayer.setSource(nullptr);
        } else {
            audioSourcePlayer.setSource(&mixerAudioSource);
        }
        return;
    }

    for (int i = 0; i < transportSources.size(); ++i) {
        if (transportSources[i] == src) {
            if (transportSources[i]->isPlaying()) {
                changeState(Playing);
            } else {
                if ((Stopping == state) || (Playing == state)) {
                    changeState(Stopped);
                } else if (Pausing == state) {
                    changeState(Paused);
                }
            }
        }
    }
    sendChangeMessage();
}

bool MultitrackPlayer::isPlaying() {
    if ((Playing == state) || (Starting == state)) {
        return true;
    }
    return false;
}

void MultitrackPlayer::playStop() {
    if (Stopped == state) {
        changeState(Starting);
    } else if (Playing == state) {
        changeState(Stopping);
    } else {
        changeState(Stopping);
    }
}

void MultitrackPlayer::setTrackTypeToPlay(mho::TrackType _trackTypeToPlay) {
    trackTypeToPlay = _trackTypeToPlay;
}

bool MultitrackPlayer::setTracks(Array <Track> tracks) {
    unloadFormerTracks();

    if (tracks.size() > 0) {
        for (int i = 0; i < tracks.size(); ++i) {
            Track track = tracks[i];
            Logger::outputDebugString("Setting track: [" + track.url + "] Pan: [" + String(track.pan) + "]");
            decideTrackToPlay(trackTypeToPlay, track);
        }
    }
    return true;
}

void MultitrackPlayer::unloadFormerTracks() {
    for (int i = 0; i < transportSources.size(); ++i) {
        mixerAudioSource.removeInputSource(channelRemappingSources[i]);
        channelRemappingSources[i]->clearAllMappings();
        channelRemappingSources[i]->releaseResources();
        channelRemappingSources.remove(i);
        
        //levelAudioSources[i]->clearAllMappings();
        gainAudioSources[i]->releaseResources();
        gainAudioSources.remove(i);
        
        transportSources[i]->removeChangeListener(this);
        transportSources[i]->setSource(nullptr);
        transportSources.remove(i);
    }
    mixerAudioSource.removeAllInputs();
    transportSources.clear();
    channelRemappingSources.clear();
    gainAudioSources.clear();
}

void MultitrackPlayer::decideTrackToPlay(const TrackType &trackTypeToPlay, const Track &track) {
    switch (track.trackType) {
        case PLAYBACK:
            decideTrackToPlayExecutor(trackTypeToPlay, track);
            break;
        case PLAYALONG_DRUMS:
            decideTrackToPlayExecutor(trackTypeToPlay, track);
            break;
        default:
            Logger::outputDebugString("... selected type NOT: " + String(int(trackTypeToPlay)) + " track type: " +
                                      String(int(track.trackType)) + " ... [setting]");
            setTrack(track);
    }
}

void MultitrackPlayer::decideTrackToPlayExecutor(const TrackType &trackTypeToPlay,
                                                 const Track &track) {
    if (track.trackType == trackTypeToPlay) {
        Logger::outputDebugString("... selected type: " + String(int(trackTypeToPlay)) + " track type: " +
                                  String(int(track.trackType)) + " ... [setting track for playback]");
        setTrack(track);
    } else {
        Logger::outputDebugString("... selected type NOT: " + String(int(trackTypeToPlay)) + " track type: " +
                                  String(int(track.trackType)) + " ... [NOT setting track for playback]");
    }
}

bool MultitrackPlayer::checkIfFileExists(const File &file) const {
    if (!file.exists()) {
        Logger::outputDebugString(
                "ERROR: File: [" + file.getFullPathName() + "] does not exist!");
        return false;
    }
    return true;
    /*
     if (!file.exists()) {
         AlertWindow::showMessageBoxAsync(
                 AlertWindow::WarningIcon,
                 "Fehler",
                 "Die Datei: [" + trackUrl + "] existiert im Verzeichnis: [" + baseUri + "] nicht!",
                 "OK"
         );
         return false;
     }
     return true;
     */
}

bool MultitrackPlayer::setTrack(const Track &track) {
    File file(track.url);

    if (!checkIfFileExists(file))
        return false;

    AudioFormatReader *audioFormatReader = formatManager.createReaderFor(file);
    StemReaderAudioTransportSource *stemReaderAudioTransportSource = new StemReaderAudioTransportSource;
    
    GainAudioSource *gainAudioSource = new GainAudioSource(stemReaderAudioTransportSource, false);
    
    ChannelRemappingAudioSource *channelRemappingAudioSource = new ChannelRemappingAudioSource(
            gainAudioSource, false);
    channelRemappingAudioSource->clearAllMappings();
    channelRemappingAudioSource->setNumberOfChannelsToProduce(2);
    
    

    Logger::outputDebugString("... channel track size: " + String(track.channels.size()));
    if (track.channels.size() <= 0) {
        // switch channels as workarounqd for old playbacks (two channels)
        Logger::outputDebugString("... NO channel set");
        Logger::outputDebugString("... set channel: [0] [1]");
        Logger::outputDebugString("... set channel: [1] [0]");

        channelRemappingAudioSource->setOutputChannelMapping(0, 1);
        channelRemappingAudioSource->setOutputChannelMapping(1, 0);
    } else {
        for (int i = 0; i < track.channels.size(); i++) {
            Channel channel = track.channels[i];
            Logger::outputDebugString(
                    "... set channel: [" + String(channel.inChannel) + "] [" + String(channel.outChannel) +
                    "]");

            channelRemappingAudioSource->setInputChannelMapping(i, i);
            channelRemappingAudioSource->setOutputChannelMapping(channel.inChannel, channel.outChannel);
        }
    }

    stemReaderAudioTransportSource->addChangeListener(this);
    stemReaderAudioTransportSource->setAudioFormatReader(audioFormatReader);

    mixerAudioSource.addInputSource(channelRemappingAudioSource, false);
    transportSources.add(stemReaderAudioTransportSource);
    channelRemappingSources.add(channelRemappingAudioSource);
    gainAudioSources.add(gainAudioSource);
    return true;
}

void MultitrackPlayer::changeState(TransportState newState) {
    if (state != newState) {
        state = newState;
        switch (state) {
            case Stopped:
                Logger::outputDebugString("Stopped");
                for (int i = 0; i < transportSources.size(); ++i) {
                    transportSources[i]->setPosition(0.0);
                }
                break;
            case Starting:
                Logger::outputDebugString("Starting");
                for (int i = 0; i < transportSources.size(); ++i) {
                    transportSources[i]->start();
                }
                break;
            case Playing:
                Logger::outputDebugString("Playing");
                break;
            case Pausing:
                Logger::outputDebugString("Pausing");
                for (int i = 0; i < transportSources.size(); ++i) {
                    transportSources[i]->stop();
                }
            case Paused:
                Logger::outputDebugString("Paused");
                break;
            case Stopping:
                Logger::outputDebugString("Stopping");
                for (int i = 0; i < transportSources.size(); ++i) {
                    transportSources[i]->stop();
                }
                break;
        }
    }
}

void MultitrackPlayer::setGain(float gain) {
    for (int i=0; i< gainAudioSources.size(); i++) {
        gainAudioSources[i]->setGain(gain);
    }
    sendChangeMessage();
}

float MultitrackPlayer::getGain() {
    float gain = 0.f;
    for (int i=0; i< gainAudioSources.size(); i++) {
        gain = gainAudioSources[i]->getGain();
    }
    return gain;
}




