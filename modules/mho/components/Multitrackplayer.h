/*==============================================================================

 Copyright 2018 by Marcus Hotz - IT-PROJEKTE
 For more information visit www.marcushotz.de

 ==============================================================================*/

#pragma once

#include "../audiosources/StemReaderAudioTransportSource.h"
#include "../audiosources/GainAudioSource.h"

//==============================================================================*/
enum TrackType {
    UNKNOWN = 0,
    PLAYALONG_DRUMS = 100,
    PLAYALONG_GIT1 = 101,
    PLAYBACK = 200
};

struct Channel {
    int inChannel;
    int outChannel;

    ~Channel() {

    }
};

struct Track {
    String url;
    float pan;
    Array<Channel> channels;
    TrackType trackType;

    ~Track() {
        channels.clear();
    }
};

class MultitrackPlayer : public Component,
                         public ChangeListener,
                         public ChangeBroadcaster {
public:

    MultitrackPlayer(AudioDeviceManager &audioDeviceManager_);

    ~MultitrackPlayer();

    void changeListenerCallback(ChangeBroadcaster *source) override;

    void playStop();

    bool isPlaying();

    void setTrackTypeToPlay(TrackType _trackTypeToPlay);

    bool setTracks(Array<Track> tracks);
                             
    void setGain(float gain);
                
    float getGain();
                        
protected:


private:
    enum TransportState {
        Stopped,
        Starting,
        Playing,
        Pausing,
        Paused,
        Stopping
    };

    AudioDeviceManager &audioDeviceManager;
    MixerAudioSource mixerAudioSource;
    AudioSourcePlayer audioSourcePlayer;
    TransportState state;

    AudioFormatManager formatManager;

    OwnedArray<StemReaderAudioTransportSource> transportSources;
    OwnedArray<ChannelRemappingAudioSource> channelRemappingSources;
    OwnedArray<GainAudioSource> gainAudioSources;

    TrackType trackTypeToPlay;

    void unloadFormerTracks();

    void decideTrackToPlay(const TrackType &trackTypeToPlay, const Track &track);

    void decideTrackToPlayExecutor(const TrackType &trackTypeToPlay, const Track &track);

    bool checkIfFileExists(const File &file) const;

    bool setTrack(const Track &track);

    void changeState(TransportState newState);

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MultitrackPlayer)

};
